import 'package:bmi/constants/app_constants.dart';
import 'package:flutter/material.dart';

class BottomReusableCard extends StatelessWidget {
  final int value;
  final String title;
  final Function() onIncreasePressed;
  final Function() onDecreasePressed;

  const BottomReusableCard({
    super.key,
    required this.value,
    required this.title,
    required this.onIncreasePressed,
    required this.onDecreasePressed,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          title,
          style: kMediumTextStyle,
        ),
        Text(
          value.toString(),
          style: kLargeTextStyle,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              backgroundColor: kActiveColor,
              radius: 24,
              child: IconButton(
                icon: Icon(
                  Icons.indeterminate_check_box_outlined,
                  color: kWhiteColor,
                ),
                onPressed: onDecreasePressed,
              ),
            ),
            SizedBox(
              width: kMediumPaMa,
            ),
            CircleAvatar(
              backgroundColor: kActiveColor,
              radius: 24,
              child: IconButton(
                icon: Icon(
                  Icons.add_box_outlined,
                  color: kWhiteColor,
                ),
                onPressed: onIncreasePressed,
              ),
            ),
          ],
        )
      ],
    );
  }
}
