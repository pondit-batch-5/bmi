import 'package:bmi/constants/app_constants.dart';
import 'package:flutter/material.dart';

class ReusableCard extends StatelessWidget {
  final Color? color;
  final Widget? widget;
  final Function()? onPressed;

  const ReusableCard({super.key, this.color, this.widget, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        padding: EdgeInsets.all(kLargePaMa),
        margin: EdgeInsets.all(kLargePaMa),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(kLargePaMa),
        ),
        child: widget,
      ),
    );
  }
}
