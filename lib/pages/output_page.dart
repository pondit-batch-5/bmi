import 'package:bmi/constants/app_constants.dart';
import 'package:flutter/material.dart';

class OutputPage extends StatelessWidget {
  final int bmi;
  final String condition;
  final String message;

  const OutputPage(
      {super.key,
      required this.bmi,
      required this.condition,
      required this.message});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: () {
            Navigator.pop(context, 200);
          },
          child: Icon(Icons.arrow_back_ios),
        ),
        title: Text('BMI RESULT'),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '$bmi',
              style: kLargeTextStyle,
            ),
            Text(
              condition,
              style: kMediumTextStyle,
            ),
            Text(
              message,
              style: kMediumTextStyle,
            ),
            InkWell(
              onTap: () {
                Navigator.pop(context, 200);
              },
              child: Container(
                padding: EdgeInsets.all(kLargePaMa),
                margin: EdgeInsets.all(kLargePaMa),
                decoration: BoxDecoration(
                  color: kActiveColor,
                  borderRadius: BorderRadius.circular(kMediumPaMa),
                ),
                child: Text(
                  'Re-Calculate BMI',
                  style: kMediumTextStyle,
                  textAlign: TextAlign.center,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
