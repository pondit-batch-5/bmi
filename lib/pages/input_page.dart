import 'package:bmi/constants/app_constants.dart';
import 'package:bmi/models/bmi.dart';
import 'package:bmi/pages/output_page.dart';
import 'package:bmi/widgets/bottom_reusable_card.dart';
import 'package:bmi/widgets/card_gender.dart';
import 'package:bmi/widgets/reusable_card.dart';
import 'package:flutter/material.dart';

class InputPage extends StatefulWidget {
  const InputPage({super.key});

  @override
  State<InputPage> createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  Gender? selectedGender;
  double height = 125.0;
  int age = 18;
  int weight = 35;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.menu),
        title: const Text('BMI Calculator'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            //gender block
            Row(
              children: [
                Expanded(
                  child: ReusableCard(
                    onPressed: () {
                      selectedGender = Gender.male;
                      setState(() {});
                    },
                    color: selectedGender == Gender.male
                        ? kActiveColor
                        : kInActiveColor,
                    widget: CardGender(
                      gender: kMale,
                      icon: Icons.male,
                    ),
                  ),
                ),
                Expanded(
                  child: ReusableCard(
                    onPressed: () {
                      selectedGender = Gender.female;
                      setState(() {});
                    },
                    color: selectedGender == Gender.female
                        ? kActiveColor
                        : kInActiveColor,
                    widget: CardGender(
                      gender: kFemale,
                      icon: Icons.female,
                    ),
                  ),
                ),
              ],
            ),
            //height
            Expanded(
              child: ReusableCard(
                onPressed: null,
                color: kInActiveColor,
                widget: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      kHeight,
                      style: kMediumTextStyle,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      textBaseline: TextBaseline.alphabetic,
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      children: [
                        Text(
                          '${height.toInt()}', //120.0 = '120.0'
                          style: kLargeTextStyle,
                        ),
                        Text(
                          kCM,
                          style: kMediumTextStyle,
                        ),
                      ],
                    ),
                    Slider(
                      value: height,
                      min: 120,
                      max: 200,
                      activeColor: kActiveColor,
                      inactiveColor: kGreyColor,
                      onChanged: (newValue) {
                        height = newValue;
                        setState(() {});
                      },
                    ),
                  ],
                ),
              ),
            ),
            //age,weight
            Expanded(
              child: ReusableCard(
                color: kInActiveColor,
                onPressed: null,
                widget: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: BottomReusableCard(
                        value: weight,
                        title: kWeight,
                        onIncreasePressed: () {
                          weight++;
                          setState(() {});
                        },
                        onDecreasePressed: () {
                          weight--;
                          setState(() {});
                        },
                      ),
                    ),
                    Expanded(
                      child: BottomReusableCard(
                        value: age,
                        title: kAge,
                        onIncreasePressed: () {
                          age++;
                          setState(() {});
                        },
                        onDecreasePressed: () {
                          age--;
                          setState(() {});
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            //button

            InkWell(
              onTap: () async {
                BMI bmiObject = BMI(height: height.toInt(), weight: weight);
                int? returnValue = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return OutputPage(
                        bmi: bmiObject.bmiResult.toInt(),
                        condition: bmiObject.bodyCondition(),
                        message: bmiObject.message(),
                      );
                    },
                  ),
                );
                if (returnValue != null && returnValue == 200) {
                  height = 125.0;
                  age = 18;
                  weight = 35;
                  setState(() {});
                }
              },
              child: Container(
                padding: EdgeInsets.all(kLargePaMa),
                margin: EdgeInsets.all(kLargePaMa),
                decoration: BoxDecoration(
                  color: kActiveColor,
                  borderRadius: BorderRadius.circular(kMediumPaMa),
                ),
                child: Text(
                  'Calculate Your BMI',
                  style: kMediumTextStyle,
                  textAlign: TextAlign.center,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
