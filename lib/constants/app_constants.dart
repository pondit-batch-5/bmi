import 'package:flutter/material.dart';

double kMediumFont = 20.0;
double kLargeFont = 50.0;
double kMediumPaMa = 8.0;
double kSmallPaMa = 4.0;
double kLargePaMa = 16.0;
Color kWhiteColor = Colors.white;
Color kGreyColor = Colors.grey;
Color kActiveColor = Colors.blue;
Color kInActiveColor = Colors.red;
String kMale = 'Male';
String kFemale = 'Female';
String kHeight = 'Height';
String kCM = 'cm';
String kAge = 'Age';
String kWeight = 'Weight';

TextStyle kMediumTextStyle = TextStyle(
  fontSize: kMediumFont,
  color: kWhiteColor,
  fontWeight: FontWeight.bold,
);

TextStyle kLargeTextStyle = TextStyle(
  fontSize: kLargeFont,
  color: kWhiteColor,
  fontWeight: FontWeight.bold,
);

enum Gender { male, female, other }
